from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render, redirect

from .models import Category, Product, Cart, Cartitem, CustomerDetail


def categories(request):
    return {
        'categories': Category.objects.all()
    }


def home(request):
    products = Product.objects.all()
    categories = Category.objects.all()
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'home.html', {'categories': categories, 'products': products, 'is_logged_in':is_logged_in})


# PRODUCTS
def all_products(request):
    products = Product.objects.all()
    categories = Category.objects.all()
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/products/product_list.html', {'categories': categories, 'products': products, 'is_logged_in':is_logged_in})


def product_detail(request, product_slug=None):
    product = get_object_or_404(Product, slug=product_slug, in_stock=True)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/products/single_product.html', {'product': product, 'is_logged_in':is_logged_in})


def product_create(request):
    if request.method == "POST":
        name = request.POST.get('product_name', '')
        price = request.POST.get('product_price', '')
        stock = request.POST.get('product_stock', '')
        description = request.POST.get('product_description', '')
        category_id = request.POST.get('product_category_id', '')
        Product.objects.create(name=name,
                               price=price,
                               stock=stock,
                               description=description,
                               category_id=category_id)
        # return reverse_lazy('product_list')
        is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/products/product_create.html',
                  context={'categories': Category.objects.all(), 'is_logged_in':is_logged_in})


def product_update(request, pk):
    if request.method == "POST":
        id_ = request.POST.get('product_id', '')
        name = request.POST.get('product_name', '')
        price = request.POST.get('product_price', '')
        stock = request.POST.get('product_stock', '')
        description = request.POST.get('product_description', '')
        category_id = request.POST.get('product_category_id', '')
        product = Product.objects.get(id=id_)
        product.name = name
        product.description = description
        product.stock = stock
        product.price = price
        product.category_id = category_id
        product.save()
        return redirect('category_product')

    product = Product.objects.get(pk=pk)
    categories = Category.objects.all()
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/products/product_update.html', {'product': product, 'categories': categories, 'is_logged_in':is_logged_in})


def product_delete(request, pk):
    if request.method == "POST":
        id_ = request.POST.get('product_id', '')
        product = Product.objects.get(id=id_)
        product.delete()

        return redirect('category_product')
    product = Product.objects.get(id=pk)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/products/product_delete.html', {'product': product, 'is_logged_in':is_logged_in})


def product_list_admin(request):
    products = Product.objects.all()
    is_logged_in = request.user is not None and not request.user.is_anonymous
    # products = Product.objects.filter(category=categories)
    return render(request, 'store/products/product_view_admin.html', {'products': products, 'is_logged_in':is_logged_in})


# CATEGORIES

def all_categories(request):
    categories = Category.objects.all()
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/category/category_list.html', {'categories': categories, 'is_logged_in':is_logged_in})


def category_list(request, category_slug=None):
    category = get_object_or_404(Category, slug=category_slug)
    products = Product.objects.filter(category=category)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/category/category_list.html', {'category': category, 'products': products, 'is_logged_in':is_logged_in})


def category_details(request, category_slug=None):
    # category = get_object_or_404(Category, slug=category_slug, )

    category = Category.objects.get(slug=category_slug)
    products = Product.objects.filter(category_id=category.id)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    print(len(products))
    return render(request, 'store/category/category_details.html', {'category': category, 'products': products,'is_logged_in':is_logged_in })


def category_create(request):
    context = {}
    error_msgs = []
    if request.method == "POST":
        print(request.POST)
        name = request.POST.get('category_name', '')

        existing_categories = Category.objects.filter(name__exact=name)
        if len(existing_categories) > 0:
            error_msgs.append('Category name exists! Please choose another name!')
        elif not name:
            error_msgs.append('Name is mandatory!')
        else:
            Category.objects.create(name=name)
            context['success_msg'] = 'Category added successfully!'
        if error_msgs:
            context['error_msgs'] = error_msgs
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/category/category_create.html',{'context':context, 'is_logged_in':is_logged_in})


def category_update(request, pk):
    context = {}
    error_msgs = []
    if request.method == "POST":
        id_ = request.POST.get('category_id', '')
        name = request.POST.get('category_name', '')
        existing_categories = Category.objects.filter(name__exact=name).exclude(id=id_)
        if len(existing_categories) > 0:
            error_msgs.append('Category name exists! Please choose another name!')
        elif not name:
            error_msgs.append('Name is mandatory!')
        else:
            category = Category.objects.get(id=id_)
            category.name = name
            category.save()
            context['success_msg'] = 'Category updated successfully!'
    context['category'] = Category.objects.get(id=pk)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/category/category_update.html', {'context':context, 'is_logged_in':is_logged_in})


def category_delete(request, pk):
    if request.method == "POST":
        id_ = request.POST.get('category_id', '')
        category = Category.objects.get(id=id_)
        category.delete()
        return redirect('store:category_list')
    category = Category.objects.get(id=pk)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/category/category_delete.html', {'category': category, 'is_logged_in':is_logged_in})


def category_list_admin(request):
    categories = Category.objects.all()
    # products = Product.objects.filter(category=categories)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/category/category_view_admin.html', {'categories': categories, 'is_logged_in':is_logged_in})


# CART

def add_to_cart(request, product_id):
    user = request.user
    carts = Cart.objects.filter(user=user, status='open')
    if len(carts) == 0:
        cart = Cart.objects.create(user=user, status='open')
    else:
        cart = carts[0]
    cart_items = Cartitem.objects.filter(cart=cart, product_id=product_id)
    if len(cart_items) == 0:
        cart_item = Cartitem.objects.create(product_id=product_id, cart=cart, quantity=1)
    else:
        cart_item = cart_items[0]
        cart_item.quantity += 1
        cart_item.save()
    return redirect('store:cart_summary',)


def remove_from_cart(request, product_id):
    return redirect('/')


def cart_summary(request):
    user = request.user
    carts = Cart.objects.filter(user=user, status='open')
    if len(carts) == 0:
        cart = Cart.objects.create(user=user, status='open')
    else:
        cart = carts[0]
    if request.method == "POST":
        cart_item_id = request.POST.get('cart_item_id', '')
        quantity = request.POST.get('quantity', '')
        cart_item = Cartitem.objects.get(id=cart_item_id)
        cart_item.quantity = quantity
        cart_item.save()
    cart_items = Cartitem.objects.filter(cart=cart, )
    sum = 0
    for cart_item in cart_items:
        sum += cart_item.quantity * cart_item.product.price
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/cart/cart_summary.html', {'cart': cart, 'cart_items': cart_items, 'cart_total': sum, 'is_logged_in':is_logged_in},)


def cart_list_admin(request):
    if request.method == 'POST':
        print(request.POST)
        cart_id = request.POST.get('cart_id', '')
        cart = Cart.objects.get(id=cart_id)
        cart.status = 'shipped'
        cart.save()
    carts = Cart.objects.all()
    # products = Product.objects.filter(category=categories)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/cart/cart_view_admin.html', {'carts': carts, 'is_logged_in':is_logged_in})


def cart_details_admin(request, pk):
    cart = Cart.objects.get(id=pk)
    cart_items = Cartitem.objects.filter(cart=cart)
    is_logged_in = request.user is not None and not request.user.is_anonymous
    return render(request, 'store/cart/cart_details_admin.html', {'cart': cart, 'cart_items': cart_items, 'is_logged_in':is_logged_in})


def cart_success(request):
    user = request.user
    carts = Cart.objects.filter(user=user, status='open')
    if len(carts) == 0:
        cart = Cart.objects.create(user=user, status='open')
    else:
        cart = carts[0]
    cart.status="closed"
    cart.save()
    return render(request, 'store/cart/Cart_success.html')



#
# def search(request):
#   # query = request.POST['usr_query']
#   #  # t = loader.get_template('gtr_site/test_search_results.html')
#   # # c = Context({ 'query': query,})
#   return render(request, 'searchbox.html')
#
#
# def SearchPage(request):
#     srh = request.GET['query']
#     products = product.objects.filter(name__icontains=srh)
#     params = {'products': products, 'search': srh}
#     return render(request, 'Searchbox.html', params)


# Account


def registration(request):
    if request.method=="POST":
        username=request.POST.get('username', '')
        email=username
        password=request.POST.get('password','')
        user=User.objects.create_user(username=username, email=email, password=password)
        first_name=request.POST.get('first_name','')
        last_name=request.POST.get('last_name','')
        address1=request.POST.get('address1','')
        address2=request.POST.get('address2','')
        payment_option=request.POST.get('payment_option','')
        user.first_name=first_name
        user.last_name=last_name
        user.save()
        CustomerDetail.objects.create(address1=address1, address2=address2, payment_option=payment_option, User=user)
        return redirect('/')
    return render(request, 'registration/registration.html', )


def user_account(request):
    return render(request, 'registration/user_account.html',)



def design_inspiration(request):
    return render(request, 'design_inspiration.html', )


def furniture(request):
    return render(request, 'furniture.html', )


def accesories(request):
    return render(request, 'accesories.html', )

def glass_morphism(request):
    return render(request, 'Facelift/glass_morphism.html', )