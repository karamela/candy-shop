from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('store.urls', namespace='store')),
    path('registration/', include('django.contrib.auth.urls')),
    # path('dashboard/', include('dashboard.urls')),

    # path('basket/', include('basket.urls', namespace='basket')),
    # path('accounts/', include('django.contrib.auth.urls')),
    # path('checkout/', include('checkout.urls')),
    # path('orders/', include('orders.urls')),
]

# if settings.DEBUG:
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
